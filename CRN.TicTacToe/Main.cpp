// Cael Neary 
// Assignment 5 -Tic Tac Toe Class

#include <iostream>
#include <conio.h>
#include <string>

#include "TicTacToe.h"

using namespace std;

int main()
{
	TicTacToe* pGame = nullptr;

	while (true)
	{
		if (pGame) delete pGame;
		pGame = new TicTacToe;


		// play the game
		while (!pGame->IsOver())
		{
			pGame->DisplayMovePositions();  //  user reference for UI
			pGame->DisplayBoard();

			int position = 0;
			do
			{
				cout << "Player " << pGame->GetPlayerTurn() << ", select a position (1-9): ";
				cin >> position;

				//  It was too easy to accidentally enter bad data, so made this mod for testing purposes [at least]
				//  if you don't enter an integer, bad things happen
				//  https://www.hackerearth.com/practice/notes/validating-user-input-in-c/
				if (cin.fail()) {
					cin.clear();
					cin.ignore();
					position = TicTacToe::POSITION_INVALID;
				}
			} while (!pGame->IsValidMove(position));

			pGame->Move(position);
		}

		// game over
		pGame->DisplayBoard();
		pGame->DisplayResult();


		// prompt to play again (or quit)
		char input = ' ';
		while (input != 'Y' && input != 'y')
		{
			std::cout << "Would you like to play again? (y/n): ";
			cin >> input;

			if (input == 'N' || input == 'n') return 0; // quit
		}
	}
}


