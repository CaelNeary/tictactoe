#pragma once

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;


class TicTacToe {

public:
	static const int BOARD_SIZE = 9; //  number of squares
	static const int WIN_STATES = 8;  //  row/col/diag to check
	static const int POSITION_INVALID = -1;
	//  board markers
	static const char POSITION_AVAILABLE = ' ';
	static const char PLAYER_X = 'X';
	static const char PLAYER_O = 'O';
	static const char PLAYER_NONE = ' ';

	//  https://stackoverflow.com/questions/24665959/initialize-static-const-multidimensional-array-with-inferred-dimensions-inside-c
	static constexpr const int RankWinStates[WIN_STATES][3]  {
			{0,1,2},			{3,4,5},			{6,7,8},  //  rows
			{0,3,6},			{1,4,7},			{2,5,8},  //  columns
			{0,4,8},			{2,4,6}  //  diagonals
	};


	TicTacToe();

	//  accessor/mutator methods
	//
	char* GetBoard();
	void SetBoard(char*);

	void SetNumTurns(int);
	int GetNumTurns();

	void SetPlayerTurn(char);
	char GetPlayerTurn();

	void SetWinner(char);
	char GetWinner();

	void DisplayBoard();

	//  Board reference for user
	static void DisplayMovePositions() {
		cout << " Board Positions" << endl;
		cout << "-----------------" << endl;
		cout << "  _1_|_2_|_3_" << endl;
		cout << "  _4_|_5_|_6_" << endl;
		cout << "   7 | 8 | 9" << endl;
		cout << "\n" << endl;
	}

	bool IsOver();

	bool IsValidMove(int);
	void Move(int);
	void DisplayResult();


private:

	char m_board[BOARD_SIZE];  // An array used to store the spaces of the game board. Once a player has moved in one of the positions, the space should change to an 'X' or an 'O'.

	int m_numTurns;  // This will be used to detect when the board is full (all nine spaces are taken).
	int m_numTurnsX;
	int m_numTurnsO;

	char m_playerTurn;  // This is used to track if it's X's turn or O's turn.
	char m_winner;  // This is used to check to see the winner of the game. A space should be used while the game is being played.

	char CheckRanksForWin();

};


