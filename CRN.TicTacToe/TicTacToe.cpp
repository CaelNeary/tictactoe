#include <iostream>
#include <conio.h>
#include <string>

#include <windows.h>
#include <debugapi.h>

#include "TicTacToe.h"

using namespace std;


//  constructor with no arguments (default)
TicTacToe::TicTacToe() {

	for (int i = 0; i < BOARD_SIZE; i++) {
		m_board[i] = PLAYER_NONE;
		//m_board[i] = '1' + i;
	}

	m_numTurns = 0;  //  no moves have been made at the start of the game
	m_numTurnsX = 0;
	m_numTurnsO = 0;

	m_playerTurn = 'X';  //  X always goes first
	m_winner = PLAYER_NONE;  //

//	this->DisplayMovePositions();
}


char* TicTacToe::GetBoard() { return m_board; }
void TicTacToe::SetBoard(char* board) { memcpy(m_board, board, BOARD_SIZE); }

int TicTacToe::GetNumTurns() { return m_numTurns; }
void TicTacToe::SetNumTurns(int numTurns) { m_numTurns = numTurns; }


char TicTacToe::GetWinner() { return m_winner; }
void TicTacToe::SetWinner(char winner) { m_winner = winner; }


//  Returns an 'X' or an 'O' depending on which player's turn it is
char TicTacToe::GetPlayerTurn() { return m_playerTurn; }
void TicTacToe::SetPlayerTurn(char player) { m_playerTurn = player; }


//This method will output the board to the console
void TicTacToe::DisplayBoard() {
	cout << "     |     |     " << endl;
	cout << "  " << m_board[0] << "  |  " << m_board[1] << "  |  " << m_board[2] << endl; // does 'board' need to be 'm_board' ?????????

	cout << "_____|_____|_____" << endl;
	cout << "     |     |     " << endl;

	cout << "  " << m_board[3] << "  |  " << m_board[4] << "  |  " << m_board[5] << endl;

	cout << "_____|_____|_____" << endl;
	cout << "     |     |     " << endl;

	cout << "  " << m_board[6] << "  |  " << m_board[7] << "  |  " << m_board[8] << endl;

	cout << "     |     |     " << endl << endl;
};

//  This method will output the board playing positions for user reference/assistance
//static void TicTacToe::DisplayMovePositions() {
//
//	cout << " Board Positions" << endl;
//	cout << "-----------------" << endl;
//	cout << "  _1_|_2_|_3_" << endl;
//	cout << "  _4_|_5_|_6_" << endl;
//	cout << "   7 | 8 | 9" << endl;
//	cout << "\n" << endl;
//};

//Returns true if the game is over (win or draw), otherwise false if the game is still being played
bool TicTacToe::IsOver() {
	if (m_numTurns == 0) { return false; }
	
	//  check and set m_winner
	CheckRanksForWin();

	if (m_winner == PLAYER_NONE && m_numTurns >= BOARD_SIZE ) {
		return true;
	}
	else if (m_winner != PLAYER_NONE) {
		return true;
	}
	else {
		return false;
	}

};

//
//  Gonna refer to a row, column, or diagonal as a "rank", because the synonym dictionary suggested it as a good common term

//

char TicTacToe::CheckRanksForWin() {
	m_winner = PLAYER_NONE;
	//  can't have a win in under 5 total turns
	if (m_numTurns >= 5) {
		for (int i = 0; i < WIN_STATES; i++) {

			if ((m_board[RankWinStates[i][0]] == m_board[RankWinStates[i][1]]) &&
				(m_board[RankWinStates[i][1]] == m_board[RankWinStates[i][2]])) {
				m_winner = m_board[RankWinStates[i][0]];
				break;
			}
		}
	}
	return m_winner;
}


//bool TicTacToe::CheckRank(int* rank) {
//	if ((rank[0] == rank[1]) && (rank[1] == rank[2])) { return true; }
//	else { return false; }
//}


//bool TicTacToe::CheckRank(int pos1, int pos2, int pos3) {
//	//  can't be a winning rank if any position is empty, as is the case early in the game
//	//  Transitive equality:  If a = b and b = c then a = c
////	if ((m_board[pos1] == POSITION_AVAILABLE) || (m_board[pos2] == POSITION_AVAILABLE) || (m_board[pos3] == POSITION_AVAILABLE)) { return false; }
//
//	if ((m_board[pos1] == m_board[pos2]) && (m_board[pos2] == m_board[pos3])) { return true; }
//	else { return false; }
//}


//Param: An int (1-9) representing a square on the board. Returns true if the space on the board is open, otherwise false.
bool TicTacToe::IsValidMove(int position) {
	if (m_board[position - 1] == POSITION_AVAILABLE) { return true; }
	else { return false; }

};

//Param: int (1-9) representing a square on the board. Places the current player's character in the specified position on the board.
void TicTacToe::Move(int position) {
	if (IsValidMove(position)) {
		m_board[position - 1] = GetPlayerTurn();
		m_numTurns++;

		char buffer[100];
		sprintf_s(buffer, "Number of turns: %i\n", m_numTurns);
		OutputDebugStringA(buffer);

		// after move, change current player (getplayerturn changes from 'X' to 'O' or vice versa)
		if (GetPlayerTurn() == PLAYER_X)
		{
			m_numTurnsX++;
			SetPlayerTurn(PLAYER_O);
		}
		else {
			m_numTurnsO++;
			SetPlayerTurn(PLAYER_X);
		}
	}
};

//Displays a message stating who the winning player is or if the game ended in a tie. 
void TicTacToe::DisplayResult() {
	// Result: Player (1 or 2) is the winner! or 
	// Result: This game has resulted in a draw
	if ((m_numTurns >= BOARD_SIZE ) && (m_winner == PLAYER_NONE)) {
		cout << "  Game has ended in a draw.  :(" << endl;
	}
	else {
		cout << " Player " << m_winner << " WINS!" << endl;
	}
	cout << endl;
};



